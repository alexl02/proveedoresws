/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author alex
 */
public class HibernateSession {
    public HibernateSession() {
    }
    
    public static SessionFactory getSessionFactory(){
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistryBuilder serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties());
        return configuration.buildSessionFactory(serviceRegistry.build());
        
        
        /*ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
	registry.applySettings(configuration.getProperties());
	ServiceRegistry serviceRegistry = registry.buildServiceRegistry();
	         
	return configuration.buildSessionFactory(serviceRegistry);*/
    }
    
    public static void close(SessionFactory factory){
        if(factory instanceof SessionFactoryImpl) {
            SessionFactoryImpl sf = (SessionFactoryImpl)factory;
            ConnectionProvider conn = sf.getConnectionProvider();
            
            /*if(conn instanceof C3P0ConnectionProvider) { 
                ((C3P0ConnectionProvider)conn).close(); 
            }*/
        }
        factory.close();
    }
}
