/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class EgresoDetalle implements Serializable{
    @Id @Column(name="id") Integer id;
    @Column(name="empresa") String empresa;
    @Column(name="co_documento") String coDocumento;
    @Column(name="co_movimiento") String coMovimiento;
    @Column(name="tipo_documento") String tipoDocumento;
    @Column(name="numero_documento") String numeroDocumento;
    @Column(name="tercero") String tercero;
    @Column(name="fecha_documento") String fechaDocumento;
    @Column(name="fecha_vencimiento") String fechaVencimiento;
    @Column(name="debitos") Float debitos;
    @Column(name="creditos") Float creditos;
    @Column(name="id_cuenta") String idCuenta;
    @Column(name="descripcion_cuenta") String descripcionCuenta;
    @Column(name="detalle1") String detalle1;
    @Column(name="tipo_documento_proveedor") String tipoDocumentoProveedor;
    @Column(name="numero_documento_proveedor") String numeroDocumentoProveedor;
    @Column(name="tipo_documento_cruce") String tipoDocumentoCruce;
    @Column(name="numero_documento_cruce") String numeroDocumentoCruce;
    @Column(name="detalle_documento") String detalleDocumento;
    @Column(name="tercero_documento") String terceroDocumento;

    public EgresoDetalle() {
    }

    public EgresoDetalle(Integer id, String empresa, String coDocumento, String coMovimiento, String tipoDocumento, String numeroDocumento, String tercero, String fechaDocumento, String fechaVencimiento, Float debitos, Float creditos, String idCuenta, String descripcionCuenta, String detalle1, String tipoDocumentoProveedor, String numeroDocumentoProveedor, String tipoDocumentoCruce, String numeroDocumentoCruce, String detalleDocumento, String terceroDocumento) {
        this.id = id;
        this.empresa = empresa;
        this.coDocumento = coDocumento;
        this.coMovimiento = coMovimiento;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.tercero = tercero;
        this.fechaDocumento = fechaDocumento;
        this.fechaVencimiento = fechaVencimiento;
        this.debitos = debitos;
        this.creditos = creditos;
        this.idCuenta = idCuenta;
        this.descripcionCuenta = descripcionCuenta;
        this.detalle1 = detalle1;
        this.tipoDocumentoProveedor = tipoDocumentoProveedor;
        this.numeroDocumentoProveedor = numeroDocumentoProveedor;
        this.tipoDocumentoCruce = tipoDocumentoCruce;
        this.numeroDocumentoCruce = numeroDocumentoCruce;
        this.detalleDocumento = detalleDocumento;
        this.terceroDocumento = terceroDocumento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCoDocumento() {
        return coDocumento;
    }

    public void setCoDocumento(String coDocumento) {
        this.coDocumento = coDocumento;
    }

    public String getCoMovimiento() {
        return coMovimiento;
    }

    public void setCoMovimiento(String coMovimiento) {
        this.coMovimiento = coMovimiento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTercero() {
        return tercero;
    }

    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    public String getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(String fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Float getDebitos() {
        return debitos;
    }

    public void setDebitos(Float debitos) {
        this.debitos = debitos;
    }

    public Float getCreditos() {
        return creditos;
    }

    public void setCreditos(Float creditos) {
        this.creditos = creditos;
    }

    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getDescripcionCuenta() {
        return descripcionCuenta;
    }

    public void setDescripcionCuenta(String descripcionCuenta) {
        this.descripcionCuenta = descripcionCuenta;
    }

    public String getDetalle1() {
        return detalle1;
    }

    public void setDetalle1(String detalle1) {
        this.detalle1 = detalle1;
    }

    public String getTipoDocumentoProveedor() {
        return tipoDocumentoProveedor;
    }

    public void setTipoDocumentoProveedor(String tipoDocumentoProveedor) {
        this.tipoDocumentoProveedor = tipoDocumentoProveedor;
    }

    public String getNumeroDocumentoProveedor() {
        return numeroDocumentoProveedor;
    }

    public void setNumeroDocumentoProveedor(String numeroDocumentoProveedor) {
        this.numeroDocumentoProveedor = numeroDocumentoProveedor;
    }

    public String getTipoDocumentoCruce() {
        return tipoDocumentoCruce;
    }

    public void setTipoDocumentoCruce(String tipoDocumentoCruce) {
        this.tipoDocumentoCruce = tipoDocumentoCruce;
    }

    public String getNumeroDocumentoCruce() {
        return numeroDocumentoCruce;
    }

    public void setNumeroDocumentoCruce(String numeroDocumentoCruce) {
        this.numeroDocumentoCruce = numeroDocumentoCruce;
    }

    public String getDetalleDocumento() {
        return detalleDocumento;
    }

    public void setDetalleDocumento(String detalleDocumento) {
        this.detalleDocumento = detalleDocumento;
    }

    public String getTerceroDocumento() {
        return terceroDocumento;
    }

    public void setTerceroDocumento(String terceroDocumento) {
        this.terceroDocumento = terceroDocumento;
    }
    
    
}
