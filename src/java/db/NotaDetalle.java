/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class NotaDetalle {
    @Id @Column(name="id") Integer id;
    @Column(name="empresa") String empresa;
    @Column(name="co") String co;
    @Column(name="fecha_doc") String fechDoc;
    @Column(name="tipo_documento") String tipoDocumento;
    @Column(name="numero_documento") String numeroDocumento;
    @Column(name="tipo_documento_causacion") String tipoDocumentoCausacion;
    @Column(name="numero_documento_causacion") String numeroDocumentoCausacion;
    @Column(name="id_item") String idItem;
    @Column(name="descripcion_item") String descripcionItem;
    @Column(name="unidad_medida") String unidadMedida;
    @Column(name="cantidad") Float cantidad;
    @Column(name="total_bruto") Float totalBruto;
    @Column(name="descuentos") Float descuentos;
    @Column(name="impuestos") Float impuestos;
    @Column(name="total_neto") Float totalNeto;

    public NotaDetalle() {
    }

    public NotaDetalle(Integer id, String empresa, String co, String fechDoc, String tipoDocumento, String numeroDocumento, String tipoDocumentoCausacion, String numeroDocumentoCausacion, String idItem, String descripcionItem, String unidadMedida, Float cantidad, Float totalBruto, Float descuentos, Float impuestos, Float totalNeto) {
        this.id = id;
        this.empresa = empresa;
        this.co = co;
        this.fechDoc = fechDoc;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumentoCausacion = tipoDocumentoCausacion;
        this.numeroDocumentoCausacion = numeroDocumentoCausacion;
        this.idItem = idItem;
        this.descripcionItem = descripcionItem;
        this.unidadMedida = unidadMedida;
        this.cantidad = cantidad;
        this.totalBruto = totalBruto;
        this.descuentos = descuentos;
        this.impuestos = impuestos;
        this.totalNeto = totalNeto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getFechDoc() {
        return fechDoc;
    }

    public void setFechDoc(String fechDoc) {
        this.fechDoc = fechDoc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumentoCausacion() {
        return tipoDocumentoCausacion;
    }

    public void setTipoDocumentoCausacion(String tipoDocumentoCausacion) {
        this.tipoDocumentoCausacion = tipoDocumentoCausacion;
    }

    public String getNumeroDocumentoCausacion() {
        return numeroDocumentoCausacion;
    }

    public void setNumeroDocumentoCausacion(String numeroDocumentoCausacion) {
        this.numeroDocumentoCausacion = numeroDocumentoCausacion;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Float getCantidad() {
        return cantidad;
    }

    public void setCantidad(Float cantidad) {
        this.cantidad = cantidad;
    }

    public Float getTotalBruto() {
        return totalBruto;
    }

    public void setTotalBruto(Float totalBruto) {
        this.totalBruto = totalBruto;
    }

    public Float getDescuentos() {
        return descuentos;
    }

    public void setDescuentos(Float descuentos) {
        this.descuentos = descuentos;
    }

    public Float getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(Float impuestos) {
        this.impuestos = impuestos;
    }

    public Float getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(Float totalNeto) {
        this.totalNeto = totalNeto;
    }

    
}
