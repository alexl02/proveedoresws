/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author alex
 */
public class PrNota {

    public PrNota() {
    }
    
    public List<NotaLista> getNotasByCC(String empresa, String co, String tipoDoc, String numeroDoc){
        List<NotaLista> ln = new ArrayList<NotaLista>();
        
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT\n" +
"	rank() over (order by MC.ID_EMP, MC.ID_CO, MC.FECHA_DCTO, MC.ID_TIPDOC, MC.DOCUMENTO_EA, MC.DOC_CS_TIPO, MC.DOCUMENTO_CS, MC.DOCUMENTO_FP) as id,\n" +
"	MC.ID_EMP as empresa,\n" +
"	MC.ID_CO as co,\n" +
"	MC.FECHA_DCTO as fecha_doc,\n" +
"	MC.ID_TIPDOC as tipo_documento,\n" +
"	MC.DOCUMENTO_EA as numero_documento,\n" +
"	MC.DOC_CS_TIPO as tipo_documento_causacion,\n" +
"	MC.DOCUMENTO_CS as numero_documento_causacion,\n" +
"	MC.DOCUMENTO_FP as documento_proveedor,\n" +
"	SUM(MC.TOT_COMPRA + MC.DSCTO_NETOS) AS total_bruto,\n" +
"	SUM(MC.TOT_COMPRA + MC.IMP_NETOS) AS total_neto\n" +
"FROM\n" +
"	CMMOVIMIENTO_COMPRAS AS MC\n" +
"WHERE\n" +
"	MC.ID_EMP = '" + empresa + "' AND \n" +
"	MC.ID_CO = '" + co + "' AND \n" +
"	MC.DOC_CS_TIPO = '" + tipoDoc + "' AND \n" +
"	MC.DOCUMENTO_CS = '" + numeroDoc + "'\n" +
"GROUP BY \n" +
"	MC.ID_EMP, \n" +
"	MC.ID_CO, \n" +
"	MC.FECHA_DCTO, \n" +
"	MC.ID_TIPDOC, \n" +
"	MC.DOCUMENTO_EA, \n" +
"	MC.DOC_CS_TIPO, \n" +
"	MC.DOCUMENTO_CS, \n" +
"	MC.DOCUMENTO_FP;";
        Query q = session.createSQLQuery(sql).addEntity(NotaLista.class);
        ln = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        
        return ln;
    }
    
    public List<Nota> getNotaByNumero(String empresa, String co, String tipoDocumento, String numeroDocumento){
        List<Nota> ln= new ArrayList<Nota>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT\n" +
"	rank() over (order by MC.ID_EMP, MC.ID_CO, MC.FECHA_DCTO, MC.ID_TIPDOC, MC.DOCUMENTO_EA, MC.DOC_CS_TIPO, MC.DOCUMENTO_CS, MC.DOCUMENTO_FP, T.DESCRIPCION) as id,\n" +
"	MC.ID_EMP as empresa,\n" +
"	MC.ID_CO as co,\n" +
"	MC.FECHA_DCTO as fecha_doc,\n" +
"	MC.ID_TIPDOC as tipo_documento,\n" +
"	MC.DOCUMENTO_EA as numero_documento,\n" +
"	CASE WHEN MI.DETALLE_DOC IS NOT NULL THEN MI.DETALLE_DOC ELSE 'SIN DETALLE' END as detalle,\n" +
"	MC.DOC_CS_TIPO as tipo_documento_causacion,\n" +
"	MC.DOCUMENTO_CS as numero_documento_causacion,\n" +
"	MC.DOCUMENTO_FP as documento_proveedor,\n" +
"	T.DESCRIPCION as tercero_descripcion,\n" +
"	SUM(MC.TOT_COMPRA + MC.DSCTO_NETOS) AS total_bruto,\n" +
"	SUM(MC.TOT_COMPRA + MC.IMP_NETOS) AS total_neto,\n" +
"	SUM(MC.DSCTO_NETOS) AS descuentos,\n" +
"	SUM(MC.IMP_NETOS) AS impuestos\n" +
"FROM\n" +
"	CMMOVIMIENTO_COMPRAS AS MC left join\n" +
"	(SELECT ID_EMP, DOC_INV_CO, DOC_INV_TIPO, DOCUMENTO_INV, DETALLE_DOC from CMMOVIMIENTO_INVENTARIO as MI GROUP BY ID_EMP, DOC_INV_CO, DOC_INV_TIPO, DOCUMENTO_INV, DETALLE_DOC) AS MI on MC.ID_EMP = MI.ID_EMP AND MC.ID_CO = MI.DOC_INV_CO and MC.ID_TIPDOC = MI.DOC_INV_TIPO and MC.DOCUMENTO_EA = MI.DOCUMENTO_INV inner join\n" +
"	TERCEROS AS T on MC.ID_TERC = T.CODIGO and MC.ID_SUC = T.SUCURSAL\n" +
"WHERE\n" +
"	MC.ID_EMP = '" + empresa + "' AND\n" +
"	MC.ID_CO = '" + co + "' AND \n" +
"	MC.ID_TIPDOC = '" + tipoDocumento + "' AND\n" +
"	MC.DOCUMENTO_EA = '" + numeroDocumento + "'\n" +
"GROUP BY\n" +
"	MC.ID_EMP,\n" +
"	MC.ID_CO,\n" +
"	MC.FECHA_DCTO,\n" +
"	MC.ID_TIPDOC,\n" +
"	MC.DOCUMENTO_EA,\n" +
"	MI.DETALLE_DOC,\n" +
"	MC.DOC_CS_TIPO,\n" +
"	MC.DOCUMENTO_CS,\n" +
"	MC.DOCUMENTO_FP,\n" +
"	T.DESCRIPCION;";
        
        Query q = session.createSQLQuery(sql).addEntity(Nota.class);
        ln = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return ln;
    }
    
    public List<NotaDetalle> getNotaDetalleByNumero(String empresa, String co, String tipoDocumento, String numeroDocumento){
        List<NotaDetalle> lnd= new ArrayList<NotaDetalle>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "select\n" +
"       rank() over (order by CMMOVIMIENTO_COMPRAS.ID_EMP, CMMOVIMIENTO_COMPRAS.ID_CO, CMMOVIMIENTO_COMPRAS.FECHA_DCTO, CMMOVIMIENTO_COMPRAS.ID_TIPDOC, CMMOVIMIENTO_COMPRAS.DOCUMENTO_EA, CMMOVIMIENTO_COMPRAS.DOC_CS_TIPO, CMMOVIMIENTO_COMPRAS.DOCUMENTO_CS, CMMOVIMIENTO_COMPRAS.ID_ITEM, ITEMS.DESCRIPCION, CMMOVIMIENTO_COMPRAS.ID_UNIDAD, CMMOVIMIENTO_COMPRAS.CANTIDAD) as id,\n" +
"	CMMOVIMIENTO_COMPRAS.ID_EMP as empresa,\n" +
"	CMMOVIMIENTO_COMPRAS.ID_CO as co,\n" +
"	CMMOVIMIENTO_COMPRAS.FECHA_DCTO as fecha_doc,\n" +
"	CMMOVIMIENTO_COMPRAS.ID_TIPDOC as tipo_documento,\n" +
"	CMMOVIMIENTO_COMPRAS.DOCUMENTO_EA as numero_documento,\n" +
"	CMMOVIMIENTO_COMPRAS.DOC_CS_TIPO as tipo_documento_causacion,\n" +
"	CMMOVIMIENTO_COMPRAS.DOCUMENTO_CS as numero_documento_causacion,\n" +
"	CMMOVIMIENTO_COMPRAS.ID_ITEM as id_item,\n" +
"	ITEMS.DESCRIPCION as descripcion_item,\n" +
"	CMMOVIMIENTO_COMPRAS.ID_UNIDAD as unidad_medida,\n" +
"	CMMOVIMIENTO_COMPRAS.CANTIDAD as cantidad,\n" +
"	case when (CMMOVIMIENTO_COMPRAS.PRECIO_UNI * CMMOVIMIENTO_COMPRAS.CANTIDAD) > 0 then (CMMOVIMIENTO_COMPRAS.PRECIO_UNI * CMMOVIMIENTO_COMPRAS.CANTIDAD) else CMMOVIMIENTO_COMPRAS.TOT_COMPRA end AS total_bruto,\n" +
"	CMMOVIMIENTO_COMPRAS.DSCTO_NETOS as descuentos,\n" +
"	CMMOVIMIENTO_COMPRAS.IMP_NETOS as impuestos,\n" +
"	(CMMOVIMIENTO_COMPRAS.TOT_COMPRA + CMMOVIMIENTO_COMPRAS.IMP_NETOS) AS total_neto\n" +
"from\n" +
"	CMMOVIMIENTO_COMPRAS,\n" +
"	ITEMS\n" +
"where\n" +
"	CMMOVIMIENTO_COMPRAS.ID_EMP = '" + empresa + "' and \n" +
"	CMMOVIMIENTO_COMPRAS.ID_CO = '" + co + "' and \n" +
"	CMMOVIMIENTO_COMPRAS.ID_TIPDOC = '" + tipoDocumento + "' and \n" +
"	CMMOVIMIENTO_COMPRAS.DOCUMENTO_EA = '" + numeroDocumento + "' and \n" +
"	CMMOVIMIENTO_COMPRAS.ID_ITEM = ITEMS.ID_ITEM;";
        
        Query q = session.createSQLQuery(sql).addEntity(NotaDetalle.class);
        lnd = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return lnd;
    }
    
    public List<Nota> getNotaByTercero(String empresa, String co, String fechaDesde, String fechaHasta, String tercero){
        List<Nota> ln= new ArrayList<Nota>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT\n" +
"	rank() over (order by MC.ID_EMP, MC.ID_CO, MC.FECHA_DCTO, MC.ID_TIPDOC, MC.DOCUMENTO_EA, MC.DOC_CS_TIPO, MC.DOCUMENTO_CS, MC.DOCUMENTO_FP) as id,\n" +
"	MC.ID_EMP as empresa,\n" +
"	MC.ID_CO as co,\n" +
"	MC.FECHA_DCTO as fecha_doc,\n" +
"	MC.ID_TIPDOC as tipo_documento,\n" +
"	MC.DOCUMENTO_EA as numero_documento,\n" +
"       '' as DETALLE,\n" +
"	MC.DOC_CS_TIPO as tipo_documento_causacion,\n" +
"	MC.DOCUMENTO_CS as numero_documento_causacion,\n" +
"	MC.DOCUMENTO_FP as documento_proveedor,\n" +
"	T.DESCRIPCION as tercero_descripcion,\n" +
"	SUM(MC.TOT_COMPRA + MC.DSCTO_NETOS) AS total_bruto,\n" +
"	SUM(MC.TOT_COMPRA + MC.IMP_NETOS) AS total_neto,\n" +
"	SUM(MC.DSCTO_NETOS) AS descuentos,\n" +
"	SUM(MC.IMP_NETOS) AS impuestos\n" +
"FROM\n" +
"	CMMOVIMIENTO_COMPRAS AS MC,\n" +
"	TERCEROS AS T\n" +
"WHERE\n" +
"	MC.ID_EMP = '" + empresa + "' AND \n" +
"	MC.ID_CO = '" + co + "' AND \n" +
"	MC.FECHA_DCTO BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "' AND \n" +
"	ID_TIPDOC = 'NP' \n" +
"	AND ID_TERC = '" + tercero + "' AND\n" +
"	MC.ID_TERC = T.CODIGO\n" +
"GROUP BY \n" +
"	MC.ID_EMP, \n" +
"	MC.ID_CO, \n" +
"	MC.FECHA_DCTO, \n" +
"	MC.ID_TIPDOC, \n" +
"	MC.DOCUMENTO_EA, \n" +
"	MC.DOC_CS_TIPO, \n" +
"	MC.DOCUMENTO_CS, \n" +
"	MC.DOCUMENTO_FP, \n" +
"	T.DESCRIPCION;";
        
        Query q = session.createSQLQuery(sql).addEntity(Nota.class);
        ln = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return ln;
    }
}
