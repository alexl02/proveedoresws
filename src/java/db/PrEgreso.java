/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author alex
 */
public class PrEgreso {

    public PrEgreso() {
    }
    
    public List<EgresoLista> getEgresoListaByTercero(String fechaDesde, String fechaHasta, String tercero){
        List<EgresoLista> lel= new ArrayList<EgresoLista>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        /*String sql = "select \n" +
"	DOC_FC_CO || DOC_FC_TIPO || DOCUMENTO_FC as id,\n" +
"	ID_EMP as empresa,\n" +
"        DOC_FC_CO as co,\n" +
"        DOC_FC_TIPO as tipo_doc,\n" +
"        DOCUMENTO_FC as numero_doc,\n" +
"        MIN(DETALLE_DOC) AS detalle,\n" +
"        FECHA_DOC as fecha_doc,\n" +
"        '' as fecha_vcto,\n" +
"        TERC as tercero,\n" +
"        SUM(VALOR_DEB) AS debito,\n" +
"        SUM(VALOR_CRE) AS credito,\n" +
"        '0' AS valor\n" +
"from\n" +
"        CGMOVIMIENTO_CONTABLE\n" +
"where\n" +
"	CGMOVIMIENTO_CONTABLE.FECHA_DOC between '" + fechaDesde + "' and '" + fechaHasta + "' and\n" +
"        CGMOVIMIENTO_CONTABLE.TERC = '" + tercero + "' and\n" +
"        DOC_FC_TIPO in ('CB', 'CE', 'EC', 'EF', 'Ec', 'Ce', 'OP', 'PE', 'PT')\n" +
"group by\n" +
"	ID_EMP,\n" +
"        DOC_FC_CO,\n" +
"        DOC_FC_TIPO,\n" +
"        DOCUMENTO_FC,\n" +
"        FECHA_DOC,\n" +
"        TERC\n" +
"order by FECHA_DOC DESC;";*/
        
        String sql = "select \n" +
"	mc.DOC_FC_CO || mc.DOC_FC_TIPO || mc.DOCUMENTO_FC as id,\n" +
"	mc.ID_EMP as empresa,\n" +
"        mc.DOC_FC_CO as co,\n" +
"        mc.DOC_FC_TIPO as tipo_doc,\n" +
"        mc.DOCUMENTO_FC as numero_doc,\n" +
"        MIN(mc.DETALLE_DOC) AS detalle,\n" +
"        mc.FECHA_DOC as fecha_doc,\n" +
"        '' as fecha_vcto,\n" +
"        mc.TERC as tercero,\n" +
"        SUM(mc.VALOR_DEB) AS debito,\n" +
"        SUM(mc.VALOR_CRE) AS credito,\n" +
"        tv.VALOR AS valor\n" +
"from\n" +
"        CGMOVIMIENTO_CONTABLE as mc inner join\n" +
"        (select \n" +
"		ID_EMP, \n" +
"		DOC_FC_CO, \n" +
"		DOC_FC_TIPO, \n" +
"		DOCUMENTO_FC, \n" +
"		sum(VALOR_CRE) as VALOR \n" +
"	from \n" +
"		CGMOVIMIENTO_CONTABLE \n" +
"	where\n" +
"		CGMOVIMIENTO_CONTABLE.FECHA_DOC between '" + fechaDesde + "' and '" + fechaHasta + "' and\n" +
"		CGMOVIMIENTO_CONTABLE.TERC = '" + tercero + "' and\n" +
"		CGMOVIMIENTO_CONTABLE.ID_CUENTA LIKE '11%'\n" +
"	group by \n" +
"		ID_EMP, DOC_FC_CO, DOC_FC_TIPO, DOCUMENTO_FC) as tv on mc.ID_EMP = tv.ID_EMP AND mc.DOC_FC_CO = tv.DOC_FC_CO AND mc.DOC_FC_TIPO = tv.DOC_FC_TIPO AND mc.DOCUMENTO_FC = tv.DOCUMENTO_FC\n" +
"where\n" +
"	mc.FECHA_DOC between '" + fechaDesde + "' and '" + fechaHasta + "' and\n" +
"        mc.TERC = '" + tercero + "' and\n" +
"        mc.DOC_FC_TIPO in ('CB', 'CE', 'EC', 'EF', 'Ec', 'Ce', 'OP', 'PE', 'PT')\n" +
"group by\n" +
"	mc.ID_EMP,\n" +
"        mc.DOC_FC_CO,\n" +
"        mc.DOC_FC_TIPO,\n" +
"        mc.DOCUMENTO_FC,\n" +
"        mc.FECHA_DOC,\n" +
"        mc.TERC,\n" +
"        tv.VALOR\n" +
"order by FECHA_DOC DESC;";
        
        Query q = session.createSQLQuery(sql).addEntity(EgresoLista.class);
        lel = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        /*for(int x=0;x<lel.size();x++){
            lel.get(x).setValor(getValorDocumento(lel.get(x).getEmpresa(),lel.get(x).getCo(),lel.get(x).getTipoDoc(),lel.get(x).getNumeroDoc()));
        }*/
        return lel;
    }
    
    public EgresoLista getEgresoListaByNumero(String empresa, String co, String tipoDocumento, String numeroDocumento){
        EgresoLista el= new EgresoLista();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "select \n" +
"	DOC_FC_CO || DOC_FC_TIPO || DOCUMENTO_FC as id,\n" +
"	ID_EMP as empresa,\n" +
"        DOC_FC_CO as co,\n" +
"        DOC_FC_TIPO as tipo_doc,\n" +
"        DOCUMENTO_FC as numero_doc,\n" +
"        MIN(DETALLE_DOC) AS detalle,\n" +
"        FECHA_DOC as fecha_doc,\n" +
"        FV.FECHA_VCTO as fecha_vcto,\n" +                
"        TERC as tercero,\n" +
"        SUM(VALOR_DEB) AS debito,\n" +
"        SUM(VALOR_CRE) AS credito,\n" +
"        '0' AS valor\n" +
"from\n" +
"        CGMOVIMIENTO_CONTABLE,\n" +
"(select FECHA_VCTO\n" +
"	from CGMOVIMIENTO_CONTABLE\n" +
"	where CGMOVIMIENTO_CONTABLE.ID_EMP = '" + empresa + "' and CGMOVIMIENTO_CONTABLE.DOC_FC_CO = '" + co +"'  and CGMOVIMIENTO_CONTABLE.DOC_FC_TIPO = '" + tipoDocumento + "' and  CGMOVIMIENTO_CONTABLE.DOCUMENTO_FC = '" + numeroDocumento + "'\n" +
"	order by FECHA_VCTO DESC limit 1) AS FV\n" +
"where\n" +
"	ID_EMP = '" + empresa + "' and\n" +
"       DOC_FC_CO = '" + co + "' and\n" +
"       DOC_FC_TIPO = '" + tipoDocumento + "' and\n" +
"       DOCUMENTO_FC = '" + numeroDocumento + "'\n" +                
"group by\n" +
"	ID_EMP,\n" +
"        DOC_FC_CO,\n" +
"        DOC_FC_TIPO,\n" +
"        DOCUMENTO_FC,\n" +
"        FECHA_DOC,\n" +
"        FV.FECHA_VCTO,\n" +
"        TERC\n" +
"order by FECHA_DOC DESC;";
        
        Query q = session.createSQLQuery(sql).addEntity(EgresoLista.class);
        el = (EgresoLista) q.uniqueResult();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        
        if(!tipoDocumento.equals("NI")){
            el.setValor(getValorDocumento(empresa, co, tipoDocumento, numeroDocumento));
        }
        
        return el;
    }
    
    public List<EgresoDetalle> getEgresoDetalleByNumero(String empresa, String co, String tipoDocumento, String numeroDocumento){
        List<EgresoDetalle> led= new ArrayList<EgresoDetalle>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "select\n" +
"	rank() over (order by ID_EMP, DOC_FC_CO, CO_MOV, DOC_FC_TIPO, DOCUMENTO_FC, TERC, FECHA_DOC, FECHA_VCTO, VALOR_DEB, VALOR_CRE, ID_CUENTA, CUENTAS_CONTAB.DESCRIPCION, DETALLE1, DOC_PRO_TIPO, D_PROVEEDOR, DOC_CRU_TIPO, D_CRUCE, DETALLE_DOC, ID_TERC_DOC) as id,\n" +
"	ID_EMP as empresa, \n" +
"	DOC_FC_CO as co_documento, \n" +
"	CO_MOV as co_movimiento, \n" +
"	DOC_FC_TIPO as tipo_documento, \n" +
"	DOCUMENTO_FC as numero_documento, \n" +
"	TERC as tercero, \n" +
"	FECHA_DOC as fecha_documento, \n" +
"	FECHA_VCTO as fecha_vencimiento, \n" +
"	VALOR_DEB as debitos, \n" +
"	VALOR_CRE as creditos, \n" +
"	ID_CUENTA as id_cuenta, \n" +
"	CUENTAS_CONTAB.DESCRIPCION as descripcion_cuenta, \n" +
"	DETALLE1 as detalle1, \n" +
"	DOC_PRO_TIPO as tipo_documento_proveedor, \n" +
"	D_PROVEEDOR as numero_documento_proveedor, \n" +
"	DOC_CRU_TIPO as tipo_documento_cruce, \n" +
"	D_CRUCE as numero_documento_cruce, \n" +
"	DETALLE_DOC as detalle_documento, \n" +
"	ID_TERC_DOC as tercero_documento\n" +
"from \n" +
"	CGMOVIMIENTO_CONTABLE, \n" +
"	CUENTAS_CONTAB\n" +
"where \n" +
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA = CUENTAS_CONTAB.CODIGO AND \n" +
"	CGMOVIMIENTO_CONTABLE.ID_EMP = '" + empresa + "' and \n" +
"	CGMOVIMIENTO_CONTABLE.DOC_FC_CO = '" + co + "' and \n" +
"	CGMOVIMIENTO_CONTABLE.DOC_FC_TIPO = '" + tipoDocumento + "' and \n" +
"	CGMOVIMIENTO_CONTABLE.DOCUMENTO_FC = '" + numeroDocumento + "';";
        
        Query q = session.createSQLQuery(sql).addEntity(EgresoDetalle.class);
        led = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return led;
    }
    
    private Float getValorDocumento(String empresa, String co, String tipoDocumento, String numeroDocumento){
        BigDecimal bValor = new BigDecimal(0);
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "select sum(VALOR_CRE) as CREDITO from CGMOVIMIENTO_CONTABLE where CGMOVIMIENTO_CONTABLE.ID_EMP = '" + empresa + "' and CGMOVIMIENTO_CONTABLE.DOC_FC_CO = '" + co + "'  and CGMOVIMIENTO_CONTABLE.DOC_FC_TIPO = '" + tipoDocumento + "' and  CGMOVIMIENTO_CONTABLE.DOCUMENTO_FC = '" + numeroDocumento + "' and CGMOVIMIENTO_CONTABLE.ID_CUENTA LIKE '11%'";
        
        Query q = session.createSQLQuery(sql);
        bValor = (BigDecimal) q.uniqueResult();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return bValor.floatValue();
    }
}
