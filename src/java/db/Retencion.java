/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class Retencion implements Serializable{
    @Id @Column(name="id") Integer id;
    @Column(name="tercero") String tercero;
    @Column(name="id_cuenta") String idCuenta;
    @Column(name="descripcion_cuenta") String descripcionCuenta;
    @Column(name="tasa") Float tasa;
    @Column(name="base") Float base;
    @Column(name="retencion") Float retencion;

    public Retencion() {
    }

    public Retencion(Integer id, String tercero, String idCuenta, String descripcionCuenta, Float tasa, Float base, Float retencion) {
        this.id = id;
        this.tercero = tercero;
        this.idCuenta = idCuenta;
        this.descripcionCuenta = descripcionCuenta;
        this.tasa = tasa;
        this.base = base;
        this.retencion = retencion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTercero() {
        return tercero;
    }

    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getDescripcionCuenta() {
        return descripcionCuenta;
    }

    public void setDescripcionCuenta(String descripcionCuenta) {
        this.descripcionCuenta = descripcionCuenta;
    }

    public Float getTasa() {
        return tasa;
    }

    public void setTasa(Float tasa) {
        this.tasa = tasa;
    }

    public Float getBase() {
        return base;
    }

    public void setBase(Float base) {
        this.base = base;
    }

    public Float getRetencion() {
        return retencion;
    }

    public void setRetencion(Float retencion) {
        this.retencion = retencion;
    }
    
    
}
