/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author alex
 */
public class PrRetencion {

    public PrRetencion() {
    }
    
    public List<Retencion> getReteFuente(String tercero, String lapsoDesde, String lapsoHasta){
        List<Retencion> lr = new ArrayList<Retencion>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT   CGMOVIMIENTO_CONTABLE.TERC as tercero,\n" +
"rank() over (order by CGMOVIMIENTO_CONTABLE.TERC, CGMOVIMIENTO_CONTABLE.ID_CUENTA, CUENTAS_CONTAB.DESCRIPCION, CGMOVIMIENTO_CONTABLE.TASA_IMPRET) as id, \n" +                
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA as id_cuenta,\n" +
"	CUENTAS_CONTAB.DESCRIPCION as descripcion_cuenta,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET as tasa,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.BASE_IVARET) AS base,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.VALOR_CRE - CGMOVIMIENTO_CONTABLE.VALOR_DEB) AS retencion\n" +
"FROM      CGMOVIMIENTO_CONTABLE INNER JOIN\n" +
"        CUENTAS_CONTAB ON CGMOVIMIENTO_CONTABLE.ID_CUENTA = CUENTAS_CONTAB.CODIGO\n" +
"WHERE     (CGMOVIMIENTO_CONTABLE.ID_CUENTA LIKE '2365%') AND\n" +
"	CGMOVIMIENTO_CONTABLE.TERC = '" + tercero + "' AND\n" +
"	CGMOVIMIENTO_CONTABLE.LAPSO_DOC BETWEEN '" + lapsoDesde + "' AND '" + lapsoHasta + "'\n" +
"GROUP BY  CGMOVIMIENTO_CONTABLE.TERC,\n" +
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA,\n" +
"	CUENTAS_CONTAB.DESCRIPCION,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET;";
        
        Query q = session.createSQLQuery(sql).addEntity(Retencion.class);
        lr = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return lr;
    }
    
    public List<Retencion> getReteIca(String tercero, String lapsoDesde, String lapsoHasta){
        List<Retencion> lr = new ArrayList<Retencion>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT   CGMOVIMIENTO_CONTABLE.TERC as tercero,\n" +
"rank() over (order by CGMOVIMIENTO_CONTABLE.TERC, CGMOVIMIENTO_CONTABLE.ID_CUENTA, CUENTAS_CONTAB.DESCRIPCION, CGMOVIMIENTO_CONTABLE.TASA_IMPRET) as id, \n" +                
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA as id_cuenta,\n" +
"	CUENTAS_CONTAB.DESCRIPCION as descripcion_cuenta,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET as tasa,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.BASE_IVARET) AS base,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.VALOR_CRE - CGMOVIMIENTO_CONTABLE.VALOR_DEB) AS retencion\n" +
"FROM      CGMOVIMIENTO_CONTABLE INNER JOIN\n" +
"        CUENTAS_CONTAB ON CGMOVIMIENTO_CONTABLE.ID_CUENTA = CUENTAS_CONTAB.CODIGO\n" +
"WHERE     (CGMOVIMIENTO_CONTABLE.ID_CUENTA LIKE '2368%') AND\n" +
"	CGMOVIMIENTO_CONTABLE.TERC = '" + tercero + "' AND\n" +
"	CGMOVIMIENTO_CONTABLE.LAPSO_DOC BETWEEN '" + lapsoDesde + "' AND '" + lapsoHasta + "'\n" +
"GROUP BY  CGMOVIMIENTO_CONTABLE.TERC,\n" +
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA,\n" +
"	CUENTAS_CONTAB.DESCRIPCION,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET;";
        
        Query q = session.createSQLQuery(sql).addEntity(Retencion.class);
        lr = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return lr;
    }
    
    public List<Retencion> getReteCree(String tercero, String lapsoDesde, String lapsoHasta){
        List<Retencion> lr = new ArrayList<Retencion>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT   CGMOVIMIENTO_CONTABLE.TERC as tercero,\n" +
"rank() over (order by CGMOVIMIENTO_CONTABLE.TERC, CGMOVIMIENTO_CONTABLE.ID_CUENTA, CUENTAS_CONTAB.DESCRIPCION, CGMOVIMIENTO_CONTABLE.TASA_IMPRET) as id, \n" +                
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA as id_cuenta,\n" +
"	CUENTAS_CONTAB.DESCRIPCION as descripcion_cuenta,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET as tasa,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.BASE_IVARET) AS base,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.VALOR_CRE - CGMOVIMIENTO_CONTABLE.VALOR_DEB) AS retencion\n" +
"FROM      CGMOVIMIENTO_CONTABLE INNER JOIN\n" +
"        CUENTAS_CONTAB ON CGMOVIMIENTO_CONTABLE.ID_CUENTA = CUENTAS_CONTAB.CODIGO\n" +
"WHERE     (CGMOVIMIENTO_CONTABLE.ID_CUENTA LIKE '2369%') AND\n" +
"	CGMOVIMIENTO_CONTABLE.TERC = '" + tercero + "' AND\n" +
"	CGMOVIMIENTO_CONTABLE.LAPSO_DOC BETWEEN '" + lapsoDesde + "' AND '" + lapsoHasta + "'\n" +
"GROUP BY  CGMOVIMIENTO_CONTABLE.TERC,\n" +
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA,\n" +
"	CUENTAS_CONTAB.DESCRIPCION,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET;";
        
        Query q = session.createSQLQuery(sql).addEntity(Retencion.class);
        lr = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return lr;
    }
    
    public List<Retencion> getReteIva(String tercero, String lapsoDesde, String lapsoHasta){
        List<Retencion> lr = new ArrayList<Retencion>();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT   CGMOVIMIENTO_CONTABLE.TERC as tercero,\n" +
"rank() over (order by CGMOVIMIENTO_CONTABLE.TERC, CGMOVIMIENTO_CONTABLE.ID_CUENTA, CUENTAS_CONTAB.DESCRIPCION, CGMOVIMIENTO_CONTABLE.TASA_IMPRET) as id, \n" +                
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA as id_cuenta,\n" +
"	CUENTAS_CONTAB.DESCRIPCION as descripcion_cuenta,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET as tasa,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.BASE_IVARET) AS base,\n" +
"	SUM(CGMOVIMIENTO_CONTABLE.VALOR_CRE - CGMOVIMIENTO_CONTABLE.VALOR_DEB) AS retencion\n" +
"FROM      CGMOVIMIENTO_CONTABLE INNER JOIN\n" +
"        CUENTAS_CONTAB ON CGMOVIMIENTO_CONTABLE.ID_CUENTA = CUENTAS_CONTAB.CODIGO\n" +
"WHERE     (CGMOVIMIENTO_CONTABLE.ID_CUENTA LIKE '2367%') AND\n" +
"	CGMOVIMIENTO_CONTABLE.TERC = '" + tercero + "' AND\n" +
"	CGMOVIMIENTO_CONTABLE.LAPSO_DOC BETWEEN '" + lapsoDesde + "' AND '" + lapsoHasta + "'\n" +
"GROUP BY  CGMOVIMIENTO_CONTABLE.TERC,\n" +
"	CGMOVIMIENTO_CONTABLE.ID_CUENTA,\n" +
"	CUENTAS_CONTAB.DESCRIPCION,\n" +
"	CGMOVIMIENTO_CONTABLE.TASA_IMPRET;";
        
        Query q = session.createSQLQuery(sql).addEntity(Retencion.class);
        lr = q.list();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return lr;
    }
    
    public Tercero getTercero(String tercero){
        Tercero t = new Tercero();
        Session session = HibernateSession.getSessionFactory().openSession();
        session.beginTransaction();
        
        String sql = "SELECT \n" +
"	CODIGO as codigo, \n" +
"	NIT_DV as digito_verificacion, \n" +
"	DESCRIPCION as descripcion, \n" +
"	DIRECCION_1 as direccion, \n" +
"	UNCIUDAD_DESCRIPCION AS ciudad \n" +
"FROM \n" +
"	TERCEROS, \n" +
"	CIUDADES \n" +
"WHERE \n" +
"	TERCEROS.CIUDAD_CORRESP = CIUDADES.ID_CIUDAD AND \n" +
"	CODIGO = '" + tercero + "';";
        
        Query q = session.createSQLQuery(sql).addEntity(Tercero.class);
        t = (Tercero) q.uniqueResult();
        
        session.getTransaction().commit();
        HibernateSession.close(session.getSessionFactory());
        return t;
    }
}
