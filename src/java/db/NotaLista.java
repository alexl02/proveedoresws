/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class NotaLista implements Serializable{
    @Id @Column(name="id") Integer id;
    @Column(name="empresa") String empresa;
    @Column(name="co") String co;
    @Column(name="fecha_doc") String fechaDoc;
    @Column(name="tipo_documento") String tipoDocumento;
    @Column(name="numero_documento") String numeroDocumento;
    @Column(name="tipo_documento_causacion") String tipoDocumentoCausacion;
    @Column(name="numero_documento_causacion") String numeroDocumentoCausacion;
    @Column(name="documento_proveedor") String documentoPorveedor;
    @Column(name="total_bruto") Float totalBruto;
    @Column(name="total_neto") Float totalNeto;

    public NotaLista() {
    }

    public NotaLista(Integer id, String empresa, String co, String fechaDoc, String tipoDocumento, String numeroDocumento, String tipoDocumentoCausacion, String numeroDocumentoCausacion, String documentoPorveedor, Float totalBruto, Float totalNeto) {
        this.id = id;
        this.empresa = empresa;
        this.co = co;
        this.fechaDoc = fechaDoc;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumentoCausacion = tipoDocumentoCausacion;
        this.numeroDocumentoCausacion = numeroDocumentoCausacion;
        this.documentoPorveedor = documentoPorveedor;
        this.totalBruto = totalBruto;
        this.totalNeto = totalNeto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(String fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumentoCausacion() {
        return tipoDocumentoCausacion;
    }

    public void setTipoDocumentoCausacion(String tipoDocumentoCausacion) {
        this.tipoDocumentoCausacion = tipoDocumentoCausacion;
    }

    public String getNumeroDocumentoCausacion() {
        return numeroDocumentoCausacion;
    }

    public void setNumeroDocumentoCausacion(String numeroDocumentoCausacion) {
        this.numeroDocumentoCausacion = numeroDocumentoCausacion;
    }

    public String getDocumentoPorveedor() {
        return documentoPorveedor;
    }

    public void setDocumentoPorveedor(String documentoPorveedor) {
        this.documentoPorveedor = documentoPorveedor;
    }

    public Float getTotalBruto() {
        return totalBruto;
    }

    public void setTotalBruto(Float totalBruto) {
        this.totalBruto = totalBruto;
    }

    public Float getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(Float totalNeto) {
        this.totalNeto = totalNeto;
    }
    
    
}
