/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class Tercero {
    @Id @Column(name="codigo") String codigo;
    @Column(name="digito_verificacion") String digitoVerificacion;
    @Column(name="descripcion") String descripcion;
    @Column(name="direccion") String direccion;
    @Column(name="ciudad") String ciudad;

    public Tercero() {
    }

    public Tercero(String codigo, String digitoVerificacion, String descripcion, String direccion, String ciudad) {
        this.codigo = codigo;
        this.digitoVerificacion = digitoVerificacion;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.ciudad = ciudad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDigitoVerificacion() {
        return digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
    
}
