/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class Nota implements Serializable{
    @Id @Column(name="id") Integer id;
    @Column(name="empresa") String empresa;
    @Column(name="co") String co;
    @Column(name="fecha_doc") String fechaDoc;
    @Column(name="tipo_documento") String tipoDocumento;
    @Column(name="numero_documento") String numeroDocumento;
    @Column(name="detalle") String detalle;
    @Column(name="tipo_documento_causacion") String tipoDocumentoCausacion;
    @Column(name="numero_documento_causacion") String numeroDocumentoCausacion;
    @Column(name="documento_proveedor") String documentoProveedor;
    @Column(name="tercero_descripcion") String terceroDescripcion;
    @Column(name="total_bruto") Float totalBruto;
    @Column(name="total_neto") Float totalNeto;
    @Column(name="descuentos") Float descuentos;
    @Column(name="impuestos") Float impuestos;

    public Nota() {
    }

    public Nota(Integer id, String empresa, String co, String fechaDoc, String tipoDocumento, String numeroDocumento, String detalle, String tipoDocumentoCausacion, String numeroDocumentoCausacion, String documentoProveedor, String terceroDescripcion, Float totalBruto, Float totalNeto, Float descuentos, Float impuestos) {
        this.id = id;
        this.empresa = empresa;
        this.co = co;
        this.fechaDoc = fechaDoc;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.detalle = detalle;
        this.tipoDocumentoCausacion = tipoDocumentoCausacion;
        this.numeroDocumentoCausacion = numeroDocumentoCausacion;
        this.documentoProveedor = documentoProveedor;
        this.terceroDescripcion = terceroDescripcion;
        this.totalBruto = totalBruto;
        this.totalNeto = totalNeto;
        this.descuentos = descuentos;
        this.impuestos = impuestos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(String fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getTipoDocumentoCausacion() {
        return tipoDocumentoCausacion;
    }

    public void setTipoDocumentoCausacion(String tipoDocumentoCausacion) {
        this.tipoDocumentoCausacion = tipoDocumentoCausacion;
    }

    public String getNumeroDocumentoCausacion() {
        return numeroDocumentoCausacion;
    }

    public void setNumeroDocumentoCausacion(String numeroDocumentoCausacion) {
        this.numeroDocumentoCausacion = numeroDocumentoCausacion;
    }

    public String getDocumentoProveedor() {
        return documentoProveedor;
    }

    public void setDocumentoProveedor(String documentoProveedor) {
        this.documentoProveedor = documentoProveedor;
    }

    public String getTerceroDescripcion() {
        return terceroDescripcion;
    }

    public void setTerceroDescripcion(String terceroDescripcion) {
        this.terceroDescripcion = terceroDescripcion;
    }

    public Float getTotalBruto() {
        return totalBruto;
    }

    public void setTotalBruto(Float totalBruto) {
        this.totalBruto = totalBruto;
    }

    public Float getTotalNeto() {
        return totalNeto;
    }

    public void setTotalNeto(Float totalNeto) {
        this.totalNeto = totalNeto;
    }

    public Float getDescuentos() {
        return descuentos;
    }

    public void setDescuentos(Float descuentos) {
        this.descuentos = descuentos;
    }

    public Float getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(Float impuestos) {
        this.impuestos = impuestos;
    }

    
}
