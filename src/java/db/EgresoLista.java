/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author alex
 */
@Entity
public class EgresoLista implements Serializable{
    @Id @Column(name="id") String id;
    @Column(name="empresa") String empresa;
    @Column(name="co") String co;
    @Column(name="tercero") String tercero;
    @Column(name="tipo_doc") String tipoDoc;
    @Column(name="numero_doc") String numeroDoc;
    @Column(name="fecha_doc") String fechaDoc;
    @Column(name="fecha_vcto") String fechaVcto;
    @Column(name="detalle") String detalle;
    @Column(name="debito") Float debito;
    @Column(name="credito") Float credito;
    @Column(name="valor") Float valor;

    public EgresoLista() {
    }

    public EgresoLista(String id, String empresa, String co, String tercero, String tipoDoc, String numeroDoc, String fechaDoc, String fechaVcto, String detalle, Float debito, Float credito, Float valor) {
        this.id = id;
        this.empresa = empresa;
        this.co = co;
        this.tercero = tercero;
        this.tipoDoc = tipoDoc;
        this.numeroDoc = numeroDoc;
        this.fechaDoc = fechaDoc;
        this.fechaVcto = fechaVcto;
        this.detalle = detalle;
        this.debito = debito;
        this.credito = credito;
        this.valor = valor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getTercero() {
        return tercero;
    }

    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(String numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(String fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getFechaVcto() {
        return fechaVcto;
    }

    public void setFechaVcto(String fechaVcto) {
        this.fechaVcto = fechaVcto;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Float getDebito() {
        return debito;
    }

    public void setDebito(Float debito) {
        this.debito = debito;
    }

    public Float getCredito() {
        return credito;
    }

    public void setCredito(Float credito) {
        this.credito = credito;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    
}
